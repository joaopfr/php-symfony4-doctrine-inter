<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Endereco;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClienteRepository")
 */
class Cliente
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @var string
     * @ORM\Column(type="string", length=20)
     */
    private $telefone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $email;


    /**
     * @var Endereco
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Endereco", cascade={"persist"})
     */
    private $endereco;

    /**
     * @var object
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Animal", inversedBy="clientes")
     */
    private $animais;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return Cliente
     */
    public function setNome(string $nome): Cliente
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefone(): ?string
    {
        return $this->telefone;
    }

    /**
     * @param string $telefone
     * @return Cliente
     */
    public function setTelefone(string $telefone): Cliente
    {
        $this->telefone = $telefone;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Cliente
     */
    public function setEmail(string $email): Cliente
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Endereco
     */
    public function getEndereco(): ?Endereco
    {
        return $this->endereco;
    }

    /**
     * @param Endereco $endereco
     * @return Cliente
     */
    public function setEndereco(Endereco $endereco): Cliente
    {
        $this->endereco = $endereco;
        return $this;
    }

    /**
     * @return object
     */
    public function getAnimais()
    {
        return $this->animais;
    }

    /**
     * @param object $animais
     * @return Cliente
     */
    public function setAnimais($animais): Cliente
    {
        $this->animais = $animais;
        return $this;
    }
}
