<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnimalRepository")
 */
class Animal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $dataNascimento;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Cliente", mappedBy="animais")
     */
    private $clientes;

    /**
     * @var Raca
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Raca", inversedBy="animais")
     */
    private $raca;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return Animal
     */
    public function setNome(string $nome): Animal
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataNascimento(): ?\DateTime
    {
        return $this->dataNascimento;
    }

    /**
     * @param \DateTime $dataNascimento
     * @return Animal
     */
    public function setDataNascimento(\DateTime $dataNascimento): Animal
    {
        $this->dataNascimento = $dataNascimento;
        return $this;
    }

    /**
     * @return object
     */
    public function getClientes()
    {
        return $this->clientes;
    }

    /**
     * @param array $clientes
     * @return Animal
     */
    public function setClientes($clientes): Animal
    {
        $this->clientes = $clientes;
        return $this;
    }

    /**
     * @return Raca
     */
    public function getRaca(): ?Raca
    {
        return $this->raca;
    }

    /**
     * @param Raca $raca
     * @return Animal
     */
    public function setRaca(Raca $raca): Animal
    {
        $this->raca = $raca;
        return $this;
    }
}
