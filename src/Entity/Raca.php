<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RacaRepository")
 */
class Raca
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Animal", mappedBy="raca")
     */
    private $animais;

    /**
     * @var Especie
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Especie")
     */
    private $especie;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     * @return Raca
     */
    public function setNome(string $nome): Raca
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return array
     */
    public function getAnimais(): ?array
    {
        return $this->animais;
    }

    /**
     * @param array $animais
     * @return Raca
     */
    public function setAnimais(array $animais): Raca
    {
        $this->animais = $animais;
        return $this;
    }

    /**
     * @return Especie
     */
    public function getEspecie(): ?Especie
    {
        return $this->especie;
    }

    /**
     * @param Especie $especie
     * @return Raca
     */
    public function setEspecie(Especie $especie): Raca
    {
        $this->especie = $especie;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNomeEspecie(): ?string
    {
        return $this->getEspecie() == null ? null : $this->getEspecie()->getNome();
    }
}
