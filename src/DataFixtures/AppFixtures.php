<?php

namespace App\DataFixtures;

use App\Entity\Cliente;
use App\Entity\Endereco;
use App\Entity\Especie;
use App\Entity\Raca;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $especies = [
            "Cachorro" => [
                "Boxer", "Shihtzu", "Poodle", "SRD"
            ],
            "Gato" => [
                "Siamês", "Angorá", "SRD"
            ]
        ];

        foreach ($especies as $especieNome => $racaNomes)
        {
            $especie = new Especie();
            $especie->setNome($especieNome);
            $manager->persist($especie);

            foreach($racaNomes as $racaNome)
            {
                $raca = new Raca();
                $raca->setNome($racaNome)
                     ->setEspecie($especie);
                $manager->persist($raca);
            }
        }

        $endereco = new Endereco();
        $endereco->setBairro("XYZ")
                 ->setRua("Rua ABC")
                 ->setNumero("123");

        $manager->persist($endereco);

        $cliente = new Cliente();
        $cliente->setNome("Alguem")
                ->setEmail("a@a.com")
                ->setTelefone("987654321")
                ->setEndereco($endereco);

        $manager->persist($cliente);

        $manager->flush();
    }
}
