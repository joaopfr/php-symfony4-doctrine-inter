<?php

namespace App\Form;

use App\Entity\Animal;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnimalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class, [
                "label" => "Nome",
                "attr" => [
                    "placeholder" => "Informe o nome do animal",
                    "class" => "form-control mb-3"
                ]
            ])
            ->add('dataNascimento', DateType::class, [
                "widget" => "choice",
                "format" => "dd-MM-yyyy",
                "label" => "Data de Nascimento",
                "attr" => [
                    "class" => "form-control mb-3"
                ]
            ])
            //->add('clientes')
            ->add('raca', EntityType::class, [
                "class" => "App\Entity\Raca",
                "choice_label" => "nome",
                "group_by" => "nomeEspecie",
                "placeholder" => "Selecione",
                "label" => "Raça",
                "attr" =>[
                    "class" => "form-control mb-3"
                ]
            ])
            ->add("salvar", SubmitType::class, [
                "attr" => ["class" => "btn btn-success"]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Animal::class,
        ]);
    }
}
