<?php

namespace App\Form;

use App\Entity\Cliente;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClienteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class, [
                "label" => "Nome",
                "attr" => [
                    "placeholder" => "Insira o nome do cliente",
                    "class" => "form-control mb-3"
                ]
            ])
            ->add('telefone', TextType::class, [
                "label" => "Telefone",
                "attr" => [
                    "placeholder" => "Insira o telefone",
                    "class" => "form-control mb-3"
                ]
            ])
            ->add('email', EmailType::class, [
                "label" => "E-mail",
                "attr" => [
                    "placeholder" => "Insira o email",
                    "class" => "form-control mb-3"
                ]
            ])
            ->add('endereco', EnderecoType::class, [
                "label" => false
            ])
            ->add('animais', EntityType::class, [
                "class" => "App\Entity\Animal",
                "choice_label" => "nome",
                "multiple" => true,
                "attr" => [
                    "class" => "form-control mb-3"
                ]
            ])
            ->add("salvar", SubmitType::class, [
                "label" => "Salvar",
                "attr" => [
                    "class" => "btn btn-success"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cliente::class,
        ]);
    }
}
