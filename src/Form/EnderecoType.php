<?php

namespace App\Form;

use App\Entity\Endereco;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EnderecoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rua', TextType::class, [
                "label" => "Rua",
                "attr" => [
                    "placeholder" => "Insira a rua",
                    "class" => "form-control mb-3"
                ]
            ])
            ->add('numero', TextType::class, [
                "label" => "Número",
                "attr" => [
                    "placeholder" => "Insira o número",
                    "class" => "form-control mb-3"
                ]
            ])
            ->add('bairro', TextType::class, [
                "label" => "Bairro",
                "attr" => [
                    "placeholder" => "Insira o bairro",
                    "class" => "form-control mb-3"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Endereco::class,
        ]);
    }
}
