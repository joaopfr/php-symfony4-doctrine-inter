<?php

namespace App\Controller;

use App\Entity\Cliente;
use App\Form\ClienteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\VarDumper\VarDumper;

class ClienteController extends AbstractController
{
    /**
     * @Route("/cliente", name="listar-clientes")
     * @Template("cliente/index.html.twig")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $clientes = $em->getRepository(Cliente::class)->findAll();
        return [
            "clientes" => $clientes,
        ];
    }

    /**
     * @param Cliente $cliente
     * @return array
     * @Route("/cliente/visualizar/{id}", name="visualizar-cliente")
     * @Template("cliente/view.html.twig")
     */
    public function view(Cliente $cliente)
    {
        return [
            "cliente" => $cliente
        ];
    }

    /**
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/cliente/cadastrar", name="cadastrar-cliente")
     * @Template("cliente/create.html.twig")
     */
    public function create(Request $request)
    {
        $cliente = new Cliente();
        $form = $this->createForm(ClienteType::class, $cliente);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cliente);
            $em->flush();

            $this->addFlash("success", "O cliente {$cliente->getNome()} foi cadastrado com sucesso");
            return $this->redirectToRoute("listar-clientes");
        }

        return [
            "form" => $form->createView()
        ];
    }
}
