<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Form\AnimalType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AnimalController extends AbstractController
{
    /**
     * @Route("/animal", name="listar-animais")
     * @Template("animal/index.html.twig")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $animais = $em->getRepository(Animal::class)->findAll();
        return [
            "animais" => $animais
        ];
    }

    /**
     * @param Animal $animal
     * @return array
     *
     * @Route("/animal/visualizar/{id}", name="visualizar-animal")
     * @Template("animal/view.html.twig")
     */
    public function view(Animal $animal)
    {
        return [
            "animal" => $animal
        ];
    }

    /**
     * @param Request $request
     * @return array
     *
     * @Route("/animal/cadastrar", name="cadastrar-animal")
     * @Template("animal/create.html.twig")
     */
    public function create(Request $request)
    {
        $animal = new Animal();
        $form = $this->createForm(AnimalType::class, $animal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($animal);
            $em->flush();

            $this->addFlash("success", "O Animal {$animal->getNome()} foi cadastrado com sucesso.");
            return $this->redirectToRoute("listar-animais");
        }

        return [
            "form" => $form->createView()
        ];
    }
}
